const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../bin/www');
const should = chai.should();

chai.use(chaiHttp);


describe('healthcheck', function() {
  it('should pass the healthcheck', function(done) {
    chai.request(server)
      .get('/healthcheck')
      .end(function(err, res){
        res.should.have.status(200);
        done();
      });
  });
});