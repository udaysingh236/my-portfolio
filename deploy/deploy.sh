#!/bin/bash

# any future command that fails will exit the script
set -e
# Lets write the public key of our aws instance
eval $(ssh-agent -s)
echo "$PRIVATE_KEY_CHECK" | tr -d '\r' | ssh-add - > /dev/null

# ** Alternative approach
# echo -e "$PRIVATE_KEY" > /root/.ssh/id_rsa
# chmod 600 /root/.ssh/id_rsa
# ** End of alternative approach

# disable the host key checking.
./deploy/disableHostKeyChecking.sh

# we have already setup the DEPLOY_SERVER in our gitlab settings.
DEPLOY_SERVER=$DEPLOY_SERVER

echo "CURRENT_BRANCH is ${CURRENT_BRANCH}"
#Deploy the changes
echo "deploying to ${DEPLOY_SERVER}"
ssh ec2-user@${DEPLOY_SERVER} 'export CURRENT_BRANCH='"'$CURRENT_BRANCH'"'; 
export CURRENT_ENV='"'$CURRENT_ENV'"';
echo "ENV is to ${CURRENT_ENV}"
rm -rf ~/my-portfolio; 
git clone --single-branch -b $CURRENT_BRANCH https://gitlab.com/udaysingh236/my-portfolio.git; 
pm2 status; 
pm2 kill;
npm remove pm2 -g;
npm install pm2 -g;
cd ~/my-portfolio;
chmod a+x ./bin/www
npm install;
pm2 start npm -- run $CURRENT_ENV -i max;
pm2 status'
